Dataset size= 25000
Hidden layer size = 25
Iterations = 100
Threads = 16

Run:	   1	Duration:	46.755406
Run:	   2	Duration:	25.087015
Run:	   3	Duration:	46.895548
Run:	   4	Duration:	46.672562
Run:	   5	Duration:	46.803742

Povprecje:
42.443 +- 3.881
