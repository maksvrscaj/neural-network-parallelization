Dataset size= 5000
Hidden layer size = 25
Iterations = 100
Threads = 8

Run:	   1	Duration:	4.907282
Run:	   2	Duration:	4.732064
Run:	   3	Duration:	4.694905
Run:	   4	Duration:	4.848062
Run:	   5	Duration:	4.828389

Povprecje:
4.802 +- 0.035
