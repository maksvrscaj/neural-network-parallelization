Dataset size= 15000
Hidden layer size = 25
Iterations = 100
Threads = 16

Run:	   1	Duration:	14.836481
Run:	   2	Duration:	20.082536
Run:	   3	Duration:	28.689177
Run:	   4	Duration:	22.774812
Run:	   5	Duration:	14.786329

Povprecje:
20.234 +- 2.339
