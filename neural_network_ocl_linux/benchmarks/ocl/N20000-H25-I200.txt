Dataset size= 20000
Hidden layer size = 25
Iterations = 200

Run:	   1	Duration:	6.438842
Run:	   2	Duration:	6.382278
Run:	   3	Duration:	6.379880
Run:	   4	Duration:	6.397513
Run:	   5	Duration:	6.385285
Run:	   6	Duration:	6.375969
Run:	   7	Duration:	6.402496
Run:	   8	Duration:	6.384062
Run:	   9	Duration:	6.378441
Run:	  10	Duration:	6.379376

Povprecje:
6.390 +- 0.006
