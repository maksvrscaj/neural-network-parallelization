Dataset size= 25000
Hidden layer size = 25
Iterations = 100
Threads = 24

Run:	   1	Duration:	12.500238
Run:	   2	Duration:	12.382885
Run:	   3	Duration:	12.377513
Run:	   4	Duration:	12.378327
Run:	   5	Duration:	12.367917
Run:	   6	Duration:	12.386367
Run:	   7	Duration:	12.385739
Run:	   8	Duration:	12.369639
Run:	   9	Duration:	12.361983
Run:	  10	Duration:	12.361438

Povprecje:
12.387 +- 0.012
