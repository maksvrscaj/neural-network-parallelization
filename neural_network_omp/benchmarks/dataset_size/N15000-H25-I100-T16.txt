Dataset size= 15000
Hidden layer size = 25
Iterations = 100
Threads = 16

Run:	   1	Duration:	11.043339
Run:	   2	Duration:	10.918094
Run:	   3	Duration:	10.395200
Run:	   4	Duration:	10.598660
Run:	   5	Duration:	10.563776
Run:	   6	Duration:	10.959443
Run:	   7	Duration:	10.556274
Run:	   8	Duration:	10.814555
Run:	   9	Duration:	10.860524
Run:	  10	Duration:	10.768033

Povprecje:
10.748 +- 0.063
