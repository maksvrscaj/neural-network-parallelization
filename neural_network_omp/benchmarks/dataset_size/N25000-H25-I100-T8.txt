Dataset size= 25000
Hidden layer size = 25
Iterations = 100
Threads = 8

Run:	   1	Duration:	18.731403
Run:	   2	Duration:	18.671523
Run:	   3	Duration:	18.687753
Run:	   4	Duration:	18.673569
Run:	   5	Duration:	18.683376
Run:	   6	Duration:	18.684935
Run:	   7	Duration:	18.686285
Run:	   8	Duration:	18.694071
Run:	   9	Duration:	18.693741
Run:	  10	Duration:	18.691210

Povprecje:
18.690 +- 0.005
